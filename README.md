# A.E.S. - Decoblocks

Decoration blocks scattered all over the server

Images under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

___________
By MisterE
___________
decoblocks_arcade_balloonbop.png
decoblocks_arcade_colourjump.png
decoblocks_arcade_sumo.png
decoblocks_arcade_tntrun.png

arcade_machine.obj (CC0, by MisterE)

___________
By MisterE and Zughy
___________
decoblocks_arcade_carpet.png

___________
By MisterE, Giov4 and Zughy

___________
decoblocks_arcade_tnttag.png

______________
EVERYTHING ELSE BY Zughy
______________
