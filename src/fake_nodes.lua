local S = minetest.get_translator("decoblocks")

minetest.register_node("decoblocks:fake_coniferous_litter", {
  description = S("[FAKE] Coniferous litter"),
  tiles = {"decoblocks_fake_coniferous_litter.png"},
  walkable = false,
  groups = {oddly_breakable_by_hand=3}
})
