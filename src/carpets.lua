local S = minetest.get_translator("decoblocks")

-- moved to the carpets mod
minetest.register_alias("decoblocks:carpet_black", "carpet:wool_black")
minetest.register_alias("decoblocks:carpet_blue", "carpet:wool_blue")
minetest.register_alias("decoblocks:carpet_brown", "carpet:wool_brown")
minetest.register_alias("decoblocks:carpet_cyan", "carpet:wool_cyan")
minetest.register_alias("decoblocks:carpet_dark_green", "carpet:wool_dark_green")
minetest.register_alias("decoblocks:carpet_dark_grey", "carpet:wool_grey")
minetest.register_alias("decoblocks:carpet_green", "carpet:wool_green")
minetest.register_alias("decoblocks:carpet_grey", "carpet:wool_grey")
minetest.register_alias("decoblocks:carpet_magenta", "carpet:wool_magenta")
minetest.register_alias("decoblocks:carpet_orange", "carpet:wool_orange")
minetest.register_alias("decoblocks:carpet_pink", "carpet:wool_pink")
minetest.register_alias("decoblocks:carpet_red", "carpet:wool_red")
minetest.register_alias("decoblocks:carpet_violet", "carpet:wool_violet")
minetest.register_alias("decoblocks:carpet_white", "carpet:wool_white")
minetest.register_alias("decoblocks:carpet_yellow", "carpet:wool_yellow")



-- arcade carpet
minetest.register_node("decoblocks:arcade", {
	description = S("Arcade carpet"),
	tiles = {{name="decoblocks_arcade_carpet.png", align_style="world", scale=8}},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.4375, 0.5}
		}
	},
	groups = {oddly_breakable_by_hand = 3}
})