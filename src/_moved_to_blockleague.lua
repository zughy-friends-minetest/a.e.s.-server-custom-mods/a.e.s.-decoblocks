-- Been moved to Block League, keeping aliases just to avoid unknown nodes
local colours = {
	"black",
	"blue",
	"dark_grey",
	"grey",
	"light_grey",
	"red"
}



for _, col in ipairs(colours) do
	minetest.register_alias("decoblocks:concrete_" .. col, "bl_decoblocks:concrete_" .. col)
  	minetest.register_alias("decoblocks:concrete_" .. col .. "_slab", "bl_decoblocks:concrete_" .. col .. "_slab")
  	minetest.register_alias("decoblocks:concrete_" .. col .. "_stair", "bl_decoblocks:concrete_" .. col .. "_stair")
end

minetest.register_alias("decoblocks:blockleague_hyperium_1", "bl_decoblocks:blockleague_hyperium_1")
minetest.register_alias("decoblocks:blockleague_hyperium_2", "bl_decoblocks:blockleague_hyperium_2")

for i = 1, 5 do
	minetest.register_alias("decoblocks:blockleague_neden1_" .. i, "bl_decoblocks:blockleague_neden1_" .. i)
end