

function decoblocks.register_arcade_machine(game,game_desc,texture)
    local name = "decoblocks:arcade_"..game
    local desc = game_desc .. " Arcade Machine"
    minetest.register_node(name, {
        drawtype = "mesh",
        mesh = "arcade_machine.obj",
        tiles = texture,
        use_texture_alpha = "clip",
        description = desc,
        paramtype = "light",
        paramtype2 = "facedir",
        selection_box = {
            type = "fixed",
            fixed = {
                {-0.4900, -0.4900, -0.3900, 0.4900, 0.3650, 0.4900}
            }
        },
        groups = {cracky = 1},
        light_source = 6,

    })
end

decoblocks.register_arcade_machine("colourjump","ColorJump",{"decoblocks_arcade_colourjump.png"})
decoblocks.register_arcade_machine("balloon_bop","Balloon Bop",{"decoblocks_arcade_balloonbop.png"})
decoblocks.register_arcade_machine("sumo","Sumo",{"decoblocks_arcade_sumo.png"})
decoblocks.register_arcade_machine("tntrun","TnT Run",{"decoblocks_arcade_tntrun.png"})
decoblocks.register_arcade_machine("tnttag","TnT Tag",{"decoblocks_arcade_tnttag.png"})



local function get_textures(c1,c2,alpha)

	return {'white.png^[colorize:'..c1,
	'white.png^[colorize:'..c2,
	'alpha'..alpha..'.png^[colorize:'..c2..':alpha',
	'alpha'..alpha..'.png^[colorize:'..c1..':alpha'
	}
end

local neon_on_step = function(c1,c2)
    return function (self, dtime, moveresult)
        if math.random(1,30) == 1 then
            local glow = math.random(1,3)
            self.object:set_properties({
                textures = get_textures(c1,c2,glow),
                glow = glow *10
                })
        end
    end
end

local neon_on_punch = function(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)
    if puncher and puncher:is_player() then
        if minetest.check_player_privs(puncher, "build") then
            self.object:set_yaw(self.object(get_yaw()+3.14159/4))
            if puncher:get_player_control().sneak == true or puncher:get_player_control().aux1 == true then
                self.object:remove()
            end
        end
    end
end

-- name, textcolor, bordercolor
local signs = {{"sumo",'#f47e1b','#f4b41b'},
    {"arcade","#394778","#3978a8"},
    {"balloonbop","#cd6093","#28ccdf"},
    {"colourjump","#28ccdf","#cd6093"},
    {"exit","#a93b3b","#e6482e"},
    {"tntrun","#e6482e","#f47e1b"},
    {"tnttag","#e6482e","#28ccdf"},
    {"up","#a93b3b","#f47e1b"},
    {"down","#a93b3b","#f47e1b"},
    {"arrow","#a93b3b","#f47e1b"},
}

for _,sign in pairs(signs) do

    minetest.register_entity('decoblocks:'..sign[1],{
        initial_properties = {
            visual = "mesh",
            mesh = sign[1]..".b3d",
            glow = 5,
            textures = get_textures(sign[2],sign[3],2),
            use_texture_alpha = true,
            collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
            visual_size = {x = 1, y = 1, z = 1},
            static_save = true,
        },
        size = vector.new(1,1,1),
        on_step = neon_on_step(sign[2],sign[3]),
        on_punch = function(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)
            if puncher and puncher:is_player() then
                if minetest.check_player_privs(puncher, "build") then
                    self.object:set_yaw(self.object:get_yaw()+3.14159/4)
                    if puncher:get_player_control().aux1 == true then
                        self.object:set_properties({visual_size = vector.add(self.object:get_properties().visual_size, 1)})
                        self.size = self.object:get_properties().visual_size
                    end
                    if puncher:get_player_control().sneak == true then
                        self.object:remove()
                    end
                end
            end
            return 0
        end,
        get_staticdata = function(self)
            return minetest.write_json({
                size = self.size,
            })
        end,

        on_activate = function (self,staticdata, dtime_s)
            if staticdata ~= "" and staticdata ~= nil then
                local data = minetest.parse_json(staticdata) or {}
                self.size = data.size
                self.object:set_properties({visual_size = data.size})
                self.object:set_armor_groups({immortal = 1, punch_operable = 1 })
            end
        end,
    })
    minetest.register_alias('neons:'..sign[1],'decoblocks:'..sign[1])
end

