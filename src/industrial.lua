local S = minetest.get_translator("decoblocks")



minetest.register_node("decoblocks:flyer", {
  description = S("Flyer"),
  tiles = {
    "decoblocks_flyer.png",
    "blank.png",
    "blank.png",
    "blank.png",
    "blank.png",
    "blank.png"
  },
  use_texture_alpha = "clip",
  drawtype = "nodebox",
  paramtype = "light",
  paramtype2 = "facedir",
  node_box = {
    type = "fixed",
    fixed = {
      {-0.5, -0.5, -0.5, 0.5, -0.49, 0.5}
    }
  },
  groups = {oddly_breakable_by_hand = 3}
})


minetest.register_alias("flyer", "decoblocks:flyer")
