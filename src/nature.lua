local S = minetest.get_translator("decoblocks")



minetest.register_node("decoblocks:beaten_path", {
  description = S("Beaten path"),
  tiles = {"decoblocks_beatenpath.png"},
  drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.4375, 0.5}
		}
	},
  groups = {crumbly = 3},
  sounds = default.node_sound_dirt_defaults(),
})


minetest.register_node("decoblocks:cobweb", {
  description = S("Cobweb"),
  drawtype = "signlike",
  tiles = {"decoblocks_cobweb.png"},
  inventory_image = "decoblocks_cobweb.png",
  wield_image = "decoblocks_cobweb.png",
  walkable = false,
  paramtype = "light",
	paramtype2 = "wallmounted",
  sunlight_propagates = true,
  selection_box = {type = "wallmounted"},
  groups = {snappy=3, flammable=3},
  sounds = default.node_sound_leaves_defaults(),

  drop = "decoblocks:string"
})



minetest.register_craftitem("decoblocks:string", {
  description = S("String"),
  inventory_image = "decoblocks_string.png"
})



minetest.register_node("decoblocks:jack_o_lantern", {
  description = S("Jack-o'-lantern"),
  drawtype = "nodebox",
  tiles = {"decoblocks_cobweb.png"},
  inventory_image = "decoblocks_cobweb.png",
  wield_image = "decoblocks_cobweb.png",
  walkable = false,
  paramtype = "light",
  sunlight_propagates = true,
  selection_box = {type = "wallmounted"},
  groups = {snappy=3, flammable=3},
  sounds = default.node_sound_leaves_defaults(),

  drop = "decoblocks:string"
})



minetest.register_node("decoblocks:jack_o_lantern", {
  description = S("Jack-o'-lantern"),
  tiles = {
    "decoblocks_jack_o_lantern_top.png",
    "decoblocks_jack_o_lantern_side.png",
    "decoblocks_jack_o_lantern_side.png",
    "decoblocks_jack_o_lantern_side.png",
    "decoblocks_jack_o_lantern_side.png",
    "decoblocks_jack_o_lantern_front.png",
  },
  paramtype2 = "facedir",
  paramtype = "light",
  light_source = 10,
  groups = {oddly_breakable_by_hand = 3}
})



minetest.register_alias("beaten_path", "decoblocks:beaten_path")
