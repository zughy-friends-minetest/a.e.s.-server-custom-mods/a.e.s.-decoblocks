local S = minetest.get_translator("decoblocks")

minetest.register_node("decoblocks:wool_blue_light", {
description = S("Light blue wool"),
tiles = {
	"wool_blue_light.png"
},
drawtype = "nodebox",
groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3,
			flammable = 3, wool = 1}
})

minetest.register_alias("wool:blue_light", "decoblocks:wool_blue_light")