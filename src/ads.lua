local S = minetest.get_translator("decoblocks")



local function register_ad(def_name, name, size)
  if size == "6x2" then
    size = {-0.4173, -0.09, -0.25, 0.5835, -0.08, 0.083}
  end

  minetest.register_node("decoblocks:ad_" .. def_name, {
    description = S("AD") .. " " .. name,
    visual_scale = 6,
    tiles = {{name="decoblocks_ad_" .. def_name .. ".png"}},
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
    node_box = {
      type = "fixed",
      fixed = size
    },
    walkable = false,
    groups = {oddly_breakable_by_hand = 3},

    on_place = function(itemstack, placer, pointed_thing)
      decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })
end



register_ad("0ad", "0 A.D.", "6x2")
register_ad("bar", "Beyond All Reason", "6x2")
register_ad("etterna", "Etterna", "6x2")
register_ad("hypersomnia", "Hypersomnia", "6x2")
register_ad("mindustry", "Mindustry", "6x2")
register_ad("speeddreams", "Speed Dreams", "6x2")
register_ad("stk", "Super Tux Kart", "6x2")
register_ad("veloren", "Veloren", "6x2")
register_ad("xonotic", "Xonotic", "6x2")








-- TEMPORARY
-- Youth Hacking 4 Freedom



local yh4f_text = "decoblocks_yh4f.jpg"


minetest.register_entity('decoblocks:yh4f', {
	initial_properties = {
		visual = "cube",
		textures = {
			yh4f_text, yh4f_text, yh4f_text, yh4f_text, yh4f_text, yh4f_text
		},
		visual_size = {x = 1 * 1.77777777778, y = 1, z = 0.0001},
    collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		static_save = true,
	},

	size = vector.new(1 * 1.77777777778, 1, 0.0001),

	on_punch = function(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)
		if puncher and puncher:is_player() then
			if minetest.check_player_privs(puncher, "server") then
				self.object:set_yaw(self.object:get_yaw() + 3.14159 / 4)

				if puncher:get_player_control().aux1 == true then
					local props = {visual_size = self.object:get_properties().visual_size,}
          props.visual_size.y = props.visual_size.y + 0.2
          props.visual_size.x = props.visual_size.y * 1.77777777778
          props.visual_size.z = 0.0001
					self.object:set_properties(props)
					self.size = self.object:get_properties().visual_size
				end

				if puncher:get_player_control().sneak == true then
					self.object:remove()
				end
			end
		end

		return 0
	end,

	get_staticdata = function(self)
		return minetest.write_json({
			size = self.size,
		})
	end,

	on_activate = function(self, staticdata, dtime_s)
		if staticdata ~= "" and staticdata ~= nil then
			local data = minetest.parse_json(staticdata) or {}
			self.size = data.size
			self.object:set_properties({visual_size = data.size})
			self.object:set_armor_groups({immortal = 1, punch_operable = 1})
		end
	end,
})