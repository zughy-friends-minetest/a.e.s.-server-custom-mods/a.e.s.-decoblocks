local srcpath = minetest.get_modpath("decoblocks") .. "/src"

decoblocks = {}


dofile(srcpath .. "/_moved_to_blockleague.lua")
dofile(srcpath .. "/ads.lua")
dofile(srcpath .. "/arcade.lua")
dofile(srcpath .. "/carpets.lua")
dofile(srcpath .. "/fake_nodes.lua")
dofile(srcpath .. "/furniture.lua")
dofile(srcpath .. "/industrial.lua")
dofile(srcpath .. "/nature.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/wool.lua")